"use strict";
const { sanitizeEntity } = require("strapi-utils");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async findOneByUserId(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.notification.findOne({ userId: id });
    return sanitizeEntity(entity, { model: strapi.models.notification });
  },

  async updateByUserId(ctx) {
    const { id } = ctx.params;

    const noti = await strapi.controllers.notification.findOneByUserId(ctx);

    let entity;

    if (!noti) {
      const payload = {
        ...ctx.request.body,
        userId: id,
      };
      entity = await strapi.services.notification.create(payload);
    } else {
      entity = await strapi.services.notification.update(
        { userId: id },
        ctx.request.body
      );
    }

    return sanitizeEntity(entity, { model: strapi.models.notification });
  },
};
